import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forms',
  templateUrl: './forms.component.html',
  styleUrls: ['./forms.component.scss']
})
export class FormsComponent implements OnInit {
  sampleForm: FormGroup;
  constructor(private fb: FormBuilder, private router: Router) { }

  ngOnInit() {
    this.initForm();
  }
  initForm() {
    this.sampleForm = this.fb.group({
      name: ['', [Validators.required, Validators.maxLength(40)]],
      email: ['', [Validators.required, Validators.email]],
      phone: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(10)]],
      city: ['', [Validators.required]],
      pincode: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(6)]],
    });
  }

  onSubmit() {
    const formdata = this.sampleForm.value

    if (this.sampleForm.valid) {
      let formDetails: any = {
        name: formdata.name,
        email: formdata.email,
        city: formdata.city,
        phone: formdata.phone,
        pincode: formdata.pincode,
      }

      let showList = JSON.parse(localStorage.getItem("showList") || "[]");
      showList.push(formDetails);
      localStorage.setItem("showList", JSON.stringify(showList));
      this.router.navigate(['/home'])
    } else {
      alert('Please check the detalis and Submit again');
    }

  }
}
